/// <reference types="cypress" />

//Feature
describe('ShopBack HW',()=>{
    var account = "{{TEST_ACCOUNT}}"
    var password = "{{TEST_PASSWORD}}"
    // before(() => {     
    //      // runs once before all tests in the block
    //      //https://docs.cypress.io/api/cypress-api/cookies.html#Examples
    //      Cypress.Cookies.debug(true, { verbose: false })
    //  })    
      // after(() => {     
    //     // runs once after all tests in the block   
    // })  
    // beforeEach(() => {     
    //     // runs before each test in the block   
    // })    
    // afterEach(() => {     
    //     // runs after each test in the block   
    // }) 

    //Scenario
    it('Visits Amazon Page',()=>{
        // Visit ExecuteAutomation Website     
        cy.visit('https://www.amazon.com/?language=en_US')
        //cy.screenshot('Visits_Amazon_Page');
        //Click the login button
        cy.get('#nav-link-accountList').click()
        
        cy.get('.a-spacing-small').should('contain','Sign-In')
    })

    it('Keyin The Account And Password',()=>{
        //Typing the account
        cy.get('#ap_email')
        .clear()
        .type(account)
        .should('have.value', account)
       cy.get('#continue').click();
       //Active the cookie and keep login process 
       cy.get('h1').should('contain','Please Enable Cookies to Continue')
       cy.get('a').click()
       cy.get('#continue').click();
       //Typing the password
       cy.get('#ap_password')
       .clear()
       .type(password)
       .should('have.value', password)
       cy.get('#signInSubmit').click();
       //Click NotNow - only first time will ask.
       //cy.get('#ap-account-fixup-phone-skip-link').click()
       //Confirm the account already login
       cy.get('#nav-link-accountList-nav-line-1').should('not.have.text','Hello, Sign in')
    })

}) 